# Projeto HTML

---

Solução do teste prático.

Conhecimentos avalidados:
* Confecção HTML;
* Confecção CSS (H1> red/right/underline);
* Aplicação correta CSS inline;
* Aplicação correta CSS inteno;
* Aplicação correta CSS externo;
* Aplicação correta Regras CSS @import;
* Inserir img HTML;
* Inserir links de navegação entre as páginas;
* Montagem de estrutura em path relativo;


    projeto/
    ├── css
    │   ├── prints.css
    │   └── style.css
    ├── img
    │   └── python-tecla.jpg
    ├── index.html
    └── pages
        ├── page1.html
        ├── page2.html
        ├── page3.html
        └── page4.html

